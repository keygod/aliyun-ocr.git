package com.gk.aliyunocr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AliyunocrApplication {

    public static void main(String[] args) {
        SpringApplication.run(AliyunocrApplication.class, args);
    }

}
