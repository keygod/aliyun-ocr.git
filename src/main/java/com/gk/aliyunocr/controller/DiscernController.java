package com.gk.aliyunocr.controller;

import com.gk.aliyunocr.service.DiscernService;
import com.gk.aliyunocr.service.IdCardDiscernService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DiscernController {

    @Value(value="${testFile.path}")
    private String FILEPATH;
    @Value(value="${aliyun.ocr.idcard.host}")
    private String OCRIDCARDHOST;
    @Value(value="${aliyun.ocr.idcard.path}")
    private String OCRIDCARDPATH;
    @Value(value="${aliyun.ocr.vehicle.host}")
    private String OCRVEHICLEHOST;
    @Value(value="${aliyun.ocr.vehicle.path}")
    private String OCRVEHICLEPATH;

    @Autowired
    private IdCardDiscernService idCardDiscernService;
    @Autowired
    private DiscernService discernService;

    @RequestMapping("/idCard")
    public String idCardDiscren(@RequestParam("fileName") String fileName,@RequestParam("side") String side){
        System.out.println("收到查验身份证信息得请求");
        String path = FILEPATH +"/"+ fileName;
        String response = discernService.discern(path,OCRIDCARDHOST,OCRIDCARDPATH,side);
        return response;
    }

    @RequestMapping("/vehicle")
    public String vehicleDiscren(@RequestParam("fileName") String fileName,@RequestParam("side") String side){
        System.out.println("收到查验行驶证信息得请求");
        String path = FILEPATH +"/"+ fileName;
        String response = discernService.discern(path,OCRVEHICLEHOST,OCRVEHICLEPATH,side);
        return response;
    }

}
