package com.gk.aliyunocr.bean;

import lombok.Data;

/**
 * 身份证正面信息
 */
@Data
public class IdCardFaceDTO {
    // 姓名
    private String name;
    // 民族
    private String nationality;
    // 身份证号
    private String num;
    // 性别
    private String sex;
    // 生日
    private String birth;
    // 地址
    private String address;
    // 配置信息，同输入
    private String configStr;
    // 人脸信息
    private String faceRect;
    // 是否是复印件
    private String isFake;
    // 识别结果
    private String success;
}
