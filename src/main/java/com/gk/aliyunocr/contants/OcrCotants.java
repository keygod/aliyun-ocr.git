package com.gk.aliyunocr.contants;

/**
 * api调用阿里云ccr接口常量
 */
public class OcrCotants {

    // 调用身份证文字识别接口的地址
    public static final String OCRIDCARDHOST = "http://dm-51.data.aliyun.com";
    //调用身份证文字识别接口的路径
    public static final String OCRIDCARDPATH = "/rest/160601/ocr/ocr_idcard.json";
    // 指定的身份认证唯一标识
    public static final String OCRIDCARDAPPCODE = "";

}
