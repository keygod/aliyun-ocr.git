package com.gk.aliyunocr.service;

/**
 * @Author poison
 * @Date 2020/10/20 11:22
 */
public interface DiscernService {

    /**
     * 调用身份证识别接口
     * @return
     */
    public String discern(String path,String hostname,String hostpath,String side);
}
