package com.gk.aliyunocr.service;

/**
 * @Author poison
 * @Date 2020/10/20 11:09
 */
public interface VehicleDiscernService {

    /**
     * 调用身份证识别接口
     * @return
     */
    public String vehicleDiscern(String path,String side);
}
