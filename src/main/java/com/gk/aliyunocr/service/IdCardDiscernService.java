package com.gk.aliyunocr.service;

/**
 * 身份证识别接口
 */
public interface IdCardDiscernService {

    /**
     * 调用身份证识别接口
     * @return
     */
    public String idCardDiscern(String path,String side);

}
