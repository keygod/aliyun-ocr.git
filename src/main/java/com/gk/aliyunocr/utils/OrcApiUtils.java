package com.gk.aliyunocr.utils;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang.StringUtils;

/**
 * 调用api接口工具类
 */
public class OrcApiUtils {

    // 封装身份证文字识别请求报文体
    public static String reqIdCardBody(String path,String side) throws Exception{
        // 获取base64数据
        String imgBase64 = FileUtils.img_base64(path);
        if(StringUtils.isEmpty(imgBase64)){
            return "";
        }

        // configure配置
        JSONObject configObj = new JSONObject();
        // 身份证正面信息
        configObj.put("side", side);

        String config_str = configObj.toString();

        // 拼装请求body的json字符串
        JSONObject requestObj = new JSONObject();
        requestObj.put("image", imgBase64);
        if(configObj!=null) {
            requestObj.put("configure", config_str);
        }
        String bodys = requestObj.toString();
        return bodys;
    }

//    /**
//     * 封装行驶证请求体
//     * @param path
//     * @param side
//     * @return
//     * @throws Exception
//     */
//    public String reqVehicleBody(String path,String side) throws Exception{
//        Boolean is_old_format = false;
//        // 获取base64数据
//        String imgBase64 = FileUtils.img_base64(path);
//        if(StringUtils.isEmpty(imgBase64)){
//            return "";
//        }
//        // 拼装请求body的json字符串
//        JSONObject requestObj = new JSONObject();
//        // 身份证正面信息
//        requestObj.put("side", side);
//        String config_str = requestObj.toString();
//        try {
//            if(is_old_format) {
//                JSONObject obj = new JSONObject();
//
//                obj.put("image", getParam(50, imgBase64));
//                if(config_str.length() > 0) {
//                    obj.put("configure", getParam(50, config_str));
//                }
//                JSONArray inputArray = new JSONArray();
//                inputArray.add(obj);
//                requestObj.put("inputs", inputArray);
//            }else{
//                requestObj.put("image", imgBase64);
//                if(config_str.length() > 0) {
//                    requestObj.put("configure", config_str);
//                }
//            }
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        String bodys = requestObj.toString();
//        return bodys;
//    }
//
//    /*
//     * 获取参数的json对象
//     */
//    public static JSONObject getParam(int type, String dataValue) {
//        JSONObject obj = new JSONObject();
//        try {
//            obj.put("dataType", type);
//            obj.put("dataValue", dataValue);
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        return obj;
//    }
}
